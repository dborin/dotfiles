dotfiles
-----------------------------

### For my VIM install

* Update VIM to 8.x
    * For MacOS, use `brew install vim`
* Install pathogen [https://github.com/tpope/vim-pathogen](https://github.com/tpope/vim-pathogen)
* Install YouCompleteMe [https://github.com/Valloric/YouCompleteMe](https://github.com/Valloric/YouCompleteMe)
* Install ctrlp [https://github.com/ctrlpvim/ctrlp.vim](https://github.com/ctrlpvim/ctrlp.vim)
* Install Syntastic [https://github.com/vim-syntastic/syntastic](https://github.com/vim-syntastic/syntastic)
* Install A.L.E - for linting [https://github.com/dmerejkowsky/vim-ale](https://github.com/dmerejkowsky/vim-ale)
* Install pylint
    * On MacOS -- use `pip install pylint`
    * On Ubuntu -- use `sudo apt install pylint`

### Memory list for what I have installed on my Mac

* PrivateInternetAccess
* For mutt, install urlscan `sudo apt install -y urlscan`
* MuteMyMic
   * [https://apple.stackexchange.com/questions/175215/how-do-i-assign-a-keyboard-shortcut-to-an-applescript-i-wrote](https://apple.stackexchange.com/questions/175215/how-do-i-assign-a-keyboard-shortcut-to-an-applescript-i-wrote)
       
       For the Automator script (workflow)
       
         tell application "System Events" to key code 96 using {option down}
* Sophos
* [Dropsec and encfs](https://webdiary.com/tag/dropsec/)
