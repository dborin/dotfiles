# .bashrc
PROMPT_COMMAND=__command_prompt

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

export VIRTUAL_ENV_DISABLE_PROMPT=yes
export DH_DATA_PATH=/home/david/work/designhub/data
export DH_DJANGO_PATH=/home/david/work/designhub/django
export PYTHONPATH=$DH_DJANGO_PATH:$DH_DATA_PATH:$PYTHONPATH
export DJANGO_SETTINGS_MODULE=designhub.settings.dev

alias 1='fg %1'
alias 2='fg %2'
alias 3='fg %3'
alias 4='fg %4'
alias quit='exit'
alias bye='exit'
alias adios='exit'
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ls='ls -AFh --color'
alias ll='ls -l'
alias lt='ll -rt'
alias ld='ls -dA'
alias ggbase='git --no-pager log --graph --pretty=format:"%C(bold yellow)%h %Creset%C(cyan)(%ad)%Creset %C(bold red)%d%Creset%C(white) %s %Creset%C(green)[%cn]" --date=relative'
alias gga='ggbase --all | less; echo ""'
alias gg='ggbase -n `expr $LINES / 2`;echo ""'
alias ggs='ggbase --stat | less'
alias h='history'
alias Cat='echo;cat'
alias takeover="tmux detach -a"
alias django="django-admin"
alias redex="DJANGO_SETTINGS_MODULE=designhub.settings.dev PYTHONPATH=$HOME/work/designhub/django/ django-admin rebuild_index"
alias dhpull="git stash;git pull;git stash pop"
alias dhstart='dhstartup -d /home/david/work/designhub -g -u -m'
alias dhrestart='dhstartup -r /home/david/work/designhub -g -u -9 -m'
alias dhfaststart='dhstartup -d /home/david/work/designhub -g -u'
alias dhfastrestart='dhstartup -r /home/david/work/designhub -g -u'
alias dhkill='dhstartup -k -9'
alias myip='dig +short myip.opendns.com @resolver1.opendns.com'
alias env='/usr/bin/env | sort'

BLUE_BACKGROUND="0;44"
BLUE="0;34"
LIGHT_BLUE="0;94"
CYAN="1;36"
YELLOW="1;33"
RED="0;31"
LIGHT_RED="1;31"
BG_LIGHT_RED="1;41"
GREEN="0;32"
LIGHT_GREEN="1;32"
MAGENTA="0;35"
LIGHT_MAGENTA="1;35"
WHITE="1;37"
DARK_GREY="0:90"
LIGHT_GRAY="0;37"
DEFAULT="0"

LS_COLORS=$LS_COLORS:"di=$LIGHT_RED:ex=$LIGHT_GREEN:" ; export LS_COLORS

function get_venv {
  basename "$PYENV_VIRTUAL_ENV"
}

function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

function __command_prompt {
  local EXIT=$?;
  git_branch="[$(parse_git_branch)]"
  local git_status="$(git status 2> /dev/null)"

  local BLUE="\033[0;34m"
  local LIGHT_BLUE="\033[38;5;45m"
  local CYAN="\033[1;36m"
  local YELLOW="\033[1;33m"
  local RED="\033[0;31m"
  local LIGHT_RED="\033[1;31m"
  local BG_LIGHT_RED="\033[1;41m"
  local GREEN="\033[0;32m"
  local LIGHT_GREEN="\033[1;32m"
  local MAGENTA="\033[0;35m"
  local LIGHT_MAGENTA="\033[1;35m"
  local WHITE="\033[1;37m"
  local DARK_GREY="\033[0:90m"
  local LIGHT_GRAY="\033[0;37m"
  local DEFAULT="\033[0m"

  local GIT_BLUE="\033[0;97;44m"
  local GIT_LIGHT_BLUE="\033[1;94m"
  local GIT_CYAN="\033[0;30;106m"
  local GIT_YELLOW="\033[0;30;103m"
  local GIT_RED="\033[0;31m"
  local GIT_LIGHT_RED="\033[1;41m"
  local GIT_GREEN="\033[0;32m"
  local GIT_LIGHT_GREEN="\033[0;30;102m"
  local GIT_MAGENTA="\033[0;30;45m"
  local GIT_LIGHT_MAGENTA="\033[0;30;105m"
  local GIT_WHITE="\033[1;37m"
  local GIT_DARK_GREY="\033[0:90m"
  local GIT_LIGHT_GREY="\033[0;37m"

  PS1="\[$RED\]╔ǁ\[$GREEN\]<$(get_venv)> \[$DEFAULT\][\[$WHITE\]\u@"
  PS1="$PS1\[$YELLOW\]centos7-vm\[$DEFAULT\]] \[$CYAN\]\w\[$DEFAULT\]\n"
  PS1="$PS1╚═\[$LIGHT_BLUE\]\t\[$DEFAULT\]"

  if [[ "$EXIT" != 0 ]]; then
    PS1="$PS1 \[$RED\]$EXIT\[$DEFAULT\]"
  fi

  if [[ $git_status =~ "Changes not staged for commit" ]]; then
    PS1="$PS1 \[$GIT_LIGHT_GREEN\]$git_branch\[$DEFAULT\]"
  elif [[ $git_status =~ "Changes to be committed" ]]; then
    PS1="$PS1 \[$GIT_YELLOW\]$git_branch\[$DEFAULT\]"
  elif [[ $git_status =~ "Your branch is ahead of" ]]; then
    PS1="$PS1 \[$GIT_LIGHT_MAGENTA\]$git_branch\[$DEFAULT\]"
  elif [[ $git_status =~ "nothing added to commit but untracked files" ]]; then
    PS1="$PS1 \[$GIT_CYAN\]$git_branch\[$DEFAULT\]"
  elif [[ $git_status =~ "nothing to commit, working tree clean" ]]; then
    PS1="$PS1 \[$GIT_DARK_GREY\]$git_branch\[$DEFAULT\]"
  elif [[ -n $git_status ]]; then
    PS1="$PS1 \[$GIT_LIGHT_RED\]$git_branch\[$DEFAULT\]"
  fi

  PS1="$PS1 \\$ "
}

DH_HOME=$HOME/work/designhub

function which-venv {
    if [[ "$(parse_git_branch)" =~ "dev-1.5" ]]; then
        pyenv global venv-1.5.0
    elif [[ "$(parse_git_branch)" =~ "dev-1.6" ]]; then
        pyenv global venv-1.6.0
    fi
}

function repop {
    dropdb designhub
    createdb designhub
    if [[ -n $@ ]];then
        DJANGO_SETTINGS_MODULE=designhub.settings.dev PYTHONPATH=$DH_HOME/django:$DH_HOME/data django-admin populate --source $@
    else
        DJANGO_SETTINGS_MODULE=designhub.settings.dev PYTHONPATH=$DH_HOME/django:$DH_HOME/data django-admin populate --source demo_small
    fi
}

function poptart {
    dhkill && repop $@ && dhstart
}

function bigtart {
    local oldpwd=$PWD
    dhkill
    cd $DH_HOME
    find django python_tests -name \*.pyc -execdir rm {} \;
    git pull
    cd dhui/
    ./install_js_packages.sh
    cd ../django/
    pip install -q -r requirements.txt -r requirements_devel.txt
    repop $@
    dhfaststart
    cd $oldpwd
}

dh-data-reset() {
    local old_dir=$PWD
    dhkill
    sleep 3
    if [[ -z $1 ]]; then
        $HOME/work/scripts/import_migrate_pgdump.sh -g -f $DH_HOME/python_tests/data/current.sql
        if [[ $? != 0 ]]; then
            kill -INT $$
        fi
    else
        $HOME/work/scripts/import_migrate_pgdump.sh -g -f $1
        if [[ $? != 0 ]]; then
            kill -INT $$
        fi
    fi
    sleep 2
    dhstartup -d /home/david/work/designhub -g -u
    cd $old_dir
}

function dh-reset {
    local old_dir=$PWD
    dhkill
    sleep 3
    rm -f $DH_HOME/django/hdf_index/report.h5 | true
    cd $HOME/work/designhub
    if [[ -z $1 ]] && [[ -z $2 ]]; then
        $HOME/work/scripts/import_migrate_pgdump.sh -f $DH_HOME/python_tests/data/current.sql -B $(parse_git_branch)
    else
        git checkout $2
        $HOME/work/scripts/import_migrate_pgdump.sh -f $1 -B $2
    fi
    if [[ $? != 0 ]]; then
        kill -INT $$
    fi
    sleep 2
    dhstartup -d $DH_HOME -u -g
    cd $old_dir
}

function dh-rollback {
    local old_dir=$PWD
    dhkill
    sleep 3
    if [[ -z $1 ]]; then
        cd $HOME/work/designhub
        git checkout dev-1.5.0
        which-venv
        $HOME/work/scripts/import_migrate_pgdump.sh -R -f $HOME/work/designhub/python_tests/data/current.sql
        if [[ $? != 0 ]]; then
            kill -INT $$
        fi
    else
        $HOME/work/scripts/import_migrate_pgdump.sh -f $1 -R
        if [[ $? != 0 ]]; then
            kill -INT $$
        fi
    fi
    sleep 2
    dhstartup -d /home/david/work/designhub -g -u
    cd $old_dir
}

timesync() {
    sudo systemctl disable ntpdate >/dev/null
    sudo systemctl enable  ntpdate >/dev/null
    sudo systemctl restart ntpd
    sudo timedatectl set-ntp true
}

export PATH="/home/david/.pyenv/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/david/.local/bin:/home/david/bin"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
# pyenv global p3-spike 2.7.14

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
