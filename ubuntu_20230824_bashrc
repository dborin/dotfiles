# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

PROMPT_COMMAND=__command_prompt
# export TZ="America/Los_Angeles"

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f "$HOME"/.bashrc_aliases ]; then
    # shellcheck disable=SC1090
    . "$HOME"/.bashrc_aliases
fi

if [ -f "$HOME"/.bashrc_functions ]; then
    # shellcheck disable=SC1090
    . "$HOME"/.bashrc_functions
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# If there are multiple matches for completion, Tab should cycle through them
bind 'TAB:menu-complete'

# Display a list of the matching files
bind "set show-all-if-ambiguous on"

# Perform partial (common) completion on the first Tab press, only start
# cycling full results on the second Tab press (from bash version 5)
bind "set menu-complete-display-prefix on"

# Cycle through history based on characters already typed on the line
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

# Keep Ctrl-Left and Ctrl-Right working when the above are used
bind '"\e[1;5C":forward-word'
bind '"\e[1;5D":backward-word'

# User specific aliases and functions

export VIRTUAL_ENV_DISABLE_PROMPT=yes
export HUB_HOME=$HOME/work/hub2
export HUB_DATA_PATH=$HUB_HOME/data
export HUB_DJANGO_PATH=$HUB_HOME/django
export HUB_PYTHONPATH=$HUB_DJANGO_PATH:$HUB_DATA_PATH
export HUB_DSM="local"
export PYTHON_CONFIGURE_OPTS="--enable-shared"

BLUE_BACKGROUND="0;44"
DARK_BLUE="0;34"
BLUE="1;38;05;33"
CYAN="1;36"
LIGHT_BLUE="0;38;05;45"
YELLOW="1;33"
RED="0;31"
LIGHT_RED="1;31"
BG_LIGHT_RED="1;41"
GREEN="0;32"
LIGHT_GREEN="0;38;05;46"
MAGENTA="0;35"
LIGHT_MAGENTA="1;35"
WHITE="1;37"
DARK_GREY="0:90"
LIGHT_GRAY="0;37"
DEFAULT="0"

LS_COLORS=$LS_COLORS:"di=$BLUE_BACKGROUND:ln=$LIGHT_BLUE:ex=$LIGHT_GREEN:*.tar=$LIGHT_RED:*.gz=$LIGHT_RED"; export LS_COLORS

# shellcheck disable=SC2155
# shellcheck disable=SC2034
__command_prompt() {
    local EXIT=$?;
    local git_branch="[$(parse_git_branch)]"
    local git_status="$(git status 2> /dev/null)"

    local BLUE="\033[0;34m"
    local LIGHT_BLUE="\033[1;38;5;45m"
    local CYAN="\033[1;36m"
    local YELLOW="\033[1;33m"
    local LIGHT_YELLOW="\033[1;38;05;11m"
    local RED="\033[0;31m"
    local LIGHT_RED="\033[1;31m"
    local BG_LIGHT_RED="\033[1;41m"
    local GREEN="\033[0;32m"
    local LIGHT_GREEN="\033[1;38;05;46m"
    local LIGHT_ORANGE="\033[1;38;05;208m"
    local PROMPT_RED="\033[1;38;05;196m"
    local MAGENTA="\033[0;35m"
    local FUSCHIA="\033[1;38;05;201m"
    local WHITE="\033[1;37m"
    local DARK_GREY="\033[0:90m"
    local LIGHT_GRAY="\033[0;37m"
    local DEFAULT="\033[0m"

    local GIT_BLUE="\033[0;97;44m"
    local GIT_LIGHT_BLUE="\033[1;94m"
    local GIT_CYAN="\033[0;30;106m"
    local GIT_YELLOW="\033[0;30;103m"
    local GIT_RED="\033[0;31m"
    local GIT_LIGHT_RED="\033[1;41m"
    local GIT_GREEN="\033[0;32m"
    local GIT_LIGHT_GREEN="\033[0;30;48;5;40m"
    local GIT_MAGENTA="\033[0;30;45m"
    local GIT_LIGHT_MAGENTA="\033[0;30;48;5;200m"
    local GIT_WHITE="\033[1;37m"
    local GIT_DARK_GREY="\033[0:90m"
    local GIT_LIGHT_GREY="\033[0;37m"

#    PS1="\[$PROMPT_RED\]╔ǁ \[$LIGHT_GREEN\]<$(get_venv)> \[$DEFAULT\][\[$LIGHT_ORANGE\]\u\[$DEFAULT\]] \[$LIGHT_BLUE\]\w\[$DEFAULT\]\n"
#    PS1="$PS1\[$BLUE\]╚═>>\[$DEFAULT\] \[$WHITE\]\t\[$DEFAULT\]"
    PS1="\[$LIGHT_GREEN\]<$(get_venv)>\[$DEFAULT\] [\[$WHITE\]\u\[$LIGHT_ORANGE\]@\h\[$DEFAULT\]] \[$LIGHT_YELLOW\]$PWD\[$DEFAULT\]\n"
    PS1=$PS1"\[$WHITE\][\[$LIGHT_ORANGE\]\! \[$LIGHT_YELLOW\]\# \[$FUSCHIA\]\j\[$WHITE\]] \t\[$DEFAULT\]"

    if [[ "$EXIT" != 0 ]]; then
        PS1="$PS1 \[$LIGHT_RED\]$EXIT \[$DEFAULT\]"
    fi

    if [[ $git_status =~ "Changes not staged for commit" ]]; then
        PS1="$PS1 \[$GIT_LIGHT_GREEN\]$git_branch\[$DEFAULT\]"
    elif [[ $git_status =~ "Changes to be committed" ]]; then
        PS1="$PS1 \[$GIT_YELLOW\]$git_branch\[$DEFAULT\]"
    elif [[ $git_status =~ "Your branch is ahead of" ]]; then
        PS1="$PS1 \[$GIT_LIGHT_MAGENTA\]$git_branch\[$DEFAULT\]"
    elif [[ $git_status =~ "nothing added to commit but untracked files" ]]; then
        PS1="$PS1 \[$GIT_CYAN\]$git_branch\[$DEFAULT\]"
    elif [[ $git_status =~ "nothing to commit, working tree clean" ]]; then
        PS1="$PS1 \[$GIT_DARK_GREY\]$git_branch\[$DEFAULT\]"
    elif [[ -n $git_status ]]; then
        PS1="$PS1 \[$GIT_LIGHT_RED\]$git_branch\[$DEFAULT\]"
    fi

    PS1="$PS1 \\$ "
}

export PATH="/home/dborin/work/hub2/hubcmd/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/dborin/.local/bin:/home/dborin/bin:/snap/bin:$PATH"

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"

# shellcheck disable=SC1090
{
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/dborin/google-cloud-sdk/path.bash.inc' ]; then . '/home/dborin/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/dborin/google-cloud-sdk/completion.bash.inc' ]; then . '/home/dborin/google-cloud-sdk/completion.bash.inc'; fi

if [ -f ~/.bashrc_sos ]; then
    . ~/.bashrc_sos
fi
}
if which psql >> /dev/null; then
    export PATH="$( dirname $(readlink -f $(which psql)) ):$PATH"
fi
